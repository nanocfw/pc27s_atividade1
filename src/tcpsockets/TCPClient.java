/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tcpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java
 *
 * Autor: Marciano da Rocha Ultima modificacao: 25/03/2018
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class TCPClient
{

    public static void main(String argv[]) throws Exception
    {
        String sentence;
        String modifiedSentence;
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        sentence = inFromUser.readLine();
        for (int i = 1; i <= 1000; i++)
        {
            int timeout = -1;
            while (timeout <= 0)
                timeout = new Random().nextInt(5000);

            Socket clientSocket = new Socket("localhost", 6789);
            clientSocket.setSoTimeout(timeout);
            DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
            BufferedReader inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            try
            {
                outToServer.writeBytes(sentence.concat(": " + i) + '\n');
                modifiedSentence = inFromServer.readLine();
                System.out.println("FROM SERVER: " + modifiedSentence);
            } catch (Exception e)
            {
                e.printStackTrace();
                System.out.println(e.getMessage());
            } finally
            {
                clientSocket.close();
            }
        }
    }
}
