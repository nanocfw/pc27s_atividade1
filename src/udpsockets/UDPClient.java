package udpsockets;

/**
 * Exemplo ilustrativo de comunicacao em rede com Sockets Java
 *
 * Autor: Marciano da Rocha Última modificacao: 25/03/2018
 *
 */
import java.io.*;
import java.net.*;
import java.util.Random;

class UDPClient
{

    public static void main(String args[]) throws Exception
    {
        BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
        DatagramSocket clientSocket = new DatagramSocket();
        InetAddress IPAddress = InetAddress.getByName("localhost");
        byte[] sendData;
        byte[] receiveData = new byte[1024];
        String sentence = inFromUser.readLine();

        for (int i = 1; i <= 1000; i++)
        {
            int timeout = -1;
            while (timeout <= 0)
                timeout = new Random().nextInt(5000);

            sendData = sentence.concat(": " + i).getBytes();
            DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, 9876);
            //Dados do cliente estao inteiramente contidos no pacote
            //Servidor usarah os dados do pacote para responder no socket do cliente
            clientSocket.setSoTimeout(timeout);
            try
            {
                clientSocket.send(sendPacket);
                DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
                clientSocket.receive(receivePacket); //note que a recepção eh feita no socket cliente
                String modifiedSentence = new String(receivePacket.getData());
                System.out.println("FROM SERVER:" + modifiedSentence);
            } catch (Exception e)
            {
                e.printStackTrace();
                System.out.println(e.getMessage());
            }
        }
        clientSocket.close();
    }
}
